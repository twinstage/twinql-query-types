# twquery-types

typescript types for `twquery`

## Details

[`twquery`](https://www.npmjs.com/package/twquery) is a powerful query language and engine that simplifies the interaction with relational databases. `twquery-types` provides the Typescript types to build the query.
