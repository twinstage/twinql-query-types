import { EntityJoin } from './entity-join';
import { Filter } from './filter';
import { Pick } from './pick';
import { Sort } from './sort';

interface Clause {
  type: string;   // search select group
  joins: (string|EntityJoin)[];
  filters: Filter[];
  picks: (string|Pick)[];
  sorts?: Sort[];
  rows?: [number, number];
}

export { Clause };
