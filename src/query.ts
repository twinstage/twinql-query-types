import { Clause } from './clause';

interface Query {
  clauses: { [id: string]: Clause };
  returns: string[];
}

export { Query };
