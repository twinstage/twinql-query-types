interface Sort {
  property: string;
  order: string;    // asc nulls first asc nulls last desc nulls first desc nulls last
}

export { Sort };
