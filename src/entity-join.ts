interface EntityJoin {
  from: string;
  to: string;
  type?: string;   // inner left right full
}

export { EntityJoin };
