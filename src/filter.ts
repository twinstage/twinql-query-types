import { EntityJoin } from './entity-join';

interface Filter {
  property?: string;
  op: string;   // = != > < >= <= contains containsNot within withinNot and or not has hasNot
  values?: any[];
  filters?: Filter[];
  joins?: EntityJoin[];   // for has/hasNot
  ifEmpty?: boolean;   // what to do if the filter ends up being empty, always true, always false or leave it null
}

export { Filter };
