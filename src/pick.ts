interface Pick {
  type: string;
  alias?: string;
  hidden?: boolean;
  groupby?: boolean;
  property?: string;
  fn?: (string|any[]);
  binds?: { [key: string]: any };
}

export { Pick };
